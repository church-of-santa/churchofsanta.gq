defmodule Churchofsanta.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :discriminator, :string
      add :discord_id, :string

      timestamps()
    end

    create unique_index(:users, [:discord_id])
  end
end
