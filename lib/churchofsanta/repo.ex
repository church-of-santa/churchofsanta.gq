defmodule Churchofsanta.Repo do
  use Ecto.Repo,
    otp_app: :churchofsanta,
    adapter: Ecto.Adapters.Postgres
end
