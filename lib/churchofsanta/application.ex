defmodule Churchofsanta.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Churchofsanta.Repo,
      # Start the Telemetry supervisor
      ChurchofsantaWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Churchofsanta.PubSub},
      # Start the Endpoint (http/https)
      ChurchofsantaWeb.Endpoint
      # Start a worker by calling: Churchofsanta.Worker.start_link(arg)
      # {Churchofsanta.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Churchofsanta.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    ChurchofsantaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
