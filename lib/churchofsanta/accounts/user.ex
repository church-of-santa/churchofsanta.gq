defmodule Churchofsanta.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :discord_id, :string
    field :discriminator, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :discriminator, :discord_id])
    |> validate_required([:name, :discriminator, :discord_id])
    |> unique_constraint(:discord_id)
  end
end
