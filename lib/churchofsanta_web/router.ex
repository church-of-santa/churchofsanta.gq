defmodule ChurchofsantaWeb.Router do
  use ChurchofsantaWeb, :router

  import ChurchofsantaWeb.SessionToAssignPlug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ChurchofsantaWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers

    plug :expose_session_variable_as_assign, :current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ChurchofsantaWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/auth", ChurchofsantaWeb do
    pipe_through :browser

    get "/me", DiscordAuthController, :profile
    get "/logout", DiscordAuthController, :delete
    get "/:provider", DiscordAuthController, :request
    get "/:provider/callback", DiscordAuthController, :callback
  end
end
