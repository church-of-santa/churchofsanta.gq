defmodule ChurchofsantaWeb.SessionToAssignPlug do
  import Plug.Conn

  def expose_session_variable_as_assign(conn, variable_name) do
    session_variable = get_session(conn, variable_name)
    assign(conn, variable_name, session_variable)
  end
end
