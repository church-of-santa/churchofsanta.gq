defmodule ChurchofsantaWeb.DiscordAuthController do
  use ChurchofsantaWeb, :controller

  plug Ueberauth

  alias Churchofsanta.Accounts
  alias Churchofsanta.Accounts.User

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out")
    |> clear_session()
    |> redirect(to: "/")
  end

  @spec callback(Plug.Conn.t(), any) :: Plug.Conn.t()
  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def callback(
        %{
          assigns: %{
            ueberauth_auth: %{
              extra: %{
                raw_info: %{
                  user: %{
                    "id" => discord_id,
                    "username" => name,
                    "discriminator" => discriminator
                  }
                }
              }
            }
          }
        } = conn,
        _params
      ) do
    with {:ok, user} <-
           (case Accounts.get_discord_user(discord_id) do
              nil -> %User{}
              user -> user
            end)
           |> Accounts.update_or_insert_user(%{
             discord_id: discord_id,
             name: name,
             discriminator: discriminator
           }) do
      conn
      |> put_flash(:info, "Successfully authenticated.")
      |> configure_session(renew: true)
      |> put_session(:current_user, user)
      |> redirect(to: "/")
    else
      _ ->
        conn
        |> put_flash(:error, "Failed to save that request")
        |> redirect(to: "/")
    end
  end

  def callback(conn, _params) do
    conn
    |> put_flash(:error, "Failed to get user data from Discord")
    |> redirect(to: "/")
  end

  def profile(conn, _params) do
    render(conn, "profile.html")
  end
end
