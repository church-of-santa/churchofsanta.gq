defmodule ChurchofsantaWeb.PageController do
  use ChurchofsantaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
