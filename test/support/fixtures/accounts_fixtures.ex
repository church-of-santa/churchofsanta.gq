defmodule Churchofsanta.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Churchofsanta.Accounts` context.
  """

  @doc """
  Generate a unique user discord_id.
  """
  def unique_user_discord_id, do: "some discord_id#{System.unique_integer([:positive])}"

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        discord_id: unique_user_discord_id(),
        discriminator: "some discriminator",
        name: "some name"
      })
      |> Churchofsanta.Accounts.create_user()

    user
  end
end
